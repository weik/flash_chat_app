import 'package:flutter/material.dart';

class CustomAlertDialog extends StatelessWidget {
  CustomAlertDialog({
    this.titleText,
    this.contentText,
    this.materialTextButton,
  });

  final String titleText;
  final String contentText;
  final String materialTextButton;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        '$titleText',
        style: TextStyle(color: Colors.red),
      ),
      content: Text('$contentText'),
      actions: <Widget>[
        materialTextButton != null
            ? FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('$materialTextButton'))
            : Container(),
      ],
    );
  }
}
