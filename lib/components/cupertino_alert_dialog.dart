import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCupertinoAlertDialog extends StatelessWidget {
  CustomCupertinoAlertDialog({
    this.titleText,
    this.contentText,
    this.cupertinoTextButton,
  });

  final String titleText;
  final String contentText;
  final String cupertinoTextButton;
  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(
        '$titleText',
        style: TextStyle(color: Colors.red),
      ),
      content: Text('$contentText'),
      actions: <Widget>[
        cupertinoTextButton != null
            ? CupertinoDialogAction(
                child: Text('$cupertinoTextButton'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            : Container(),
      ],
    );
  }
}
