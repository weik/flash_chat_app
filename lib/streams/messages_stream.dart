import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../components/message_bubble.dart';
import '../screens/chat_screen.dart';

class MessagesStream extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: firestore
          .collection('messages')
          .orderBy('timestamp', descending: false)
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.lightBlueAccent,
            ),
          );
        }
        final messages = snapshot.data.documents.reversed;
        List<MessageBubble> messageBubbles = [];
        for (var message in messages) {
          final messageText = message.data['text'];

          final messageSender = message.data['sender'];

          final image = message.data['imageurl'];

          final currentUser = loggedInUser.email;

          final messageBubble = MessageBubble(
            //sender:message contains sender login
            sender: messageSender,
            //text: messageText contains message text
            text: messageText,
            //isMe: currentUser == messageSender checking for whom the message was sent
            isMe: currentUser == messageSender,
            //imageURL: image,   //image to message bubble
            imageURL: image,
          );
          messageBubbles.add(messageBubble);
        }
        return Expanded(
          child: ListView(
            reverse: true,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            children: messageBubbles,
          ),
        );
      },
    );
  }
}
